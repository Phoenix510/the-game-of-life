﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private World world;

        Grid DynamicGrid = new Grid();
        bool started = false;
        int CellSize = 10;
        Random random; 

        public MainWindow()
        {
            InitializeComponent();
            var area = new bool[106, 204];
            area[0, 1] = true;
            area[1, 1] = true;
            area[2, 1] = true;
            world = new World(area);
            random = new Random();
            RandomizeCells();

            DynamicGrid.Width = world.ColCount* CellSize;
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Left;
            DynamicGrid.VerticalAlignment = VerticalAlignment.Top;

            MainContainer.Children.Add(DynamicGrid);
            CreateGameArea();
            SetView();
        }

        void RandomizeCells()
        {
            for (int i = 0; i < world.RowsCount; i++)
            {
                for (int j = 0; j < world.ColCount; j++)
                {
                    world.Area[i, j] = random.Next(2) == 0;
                }
            }
        }

        DispatcherTimer timer;
        void StartGame()
        {
            if (!started)
            {
                started = true;
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += Timer_Tick;
                timer.Start();
                StartStopButton.Content = "Stop game";
                return;
            }

            started = false;
            StartStopButton.Content = "Start game";
            timer.Stop();
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            world.Cycle();
            SetView();
            //(sender as DispatcherTimer).Stop();
            //for (int i = 0; i < area.GetLength(0); i++)
            //{
            //    for (int j = 0; j < area.GetLength(1); j++)
            //    {
            //        if (area[i, j])
            //            (DynamicGrid.Children[(i +1) * (j +1) -1] as Rectangle).Fill = new SolidColorBrush(Colors.Green);
            //        else
            //            (DynamicGrid.Children[(i + 1) * (j + 1) -1] as Rectangle).Fill = new SolidColorBrush(Colors.Black); //= new Rectangle() { Fill = new SolidColorBrush(Colors.Black) };
            //    }
            //}
            //(sender as DispatcherTimer).Stop();
        }

        void CreateGameArea()
        {
            for(int i =0; i<world.ColCount; i++)
            {
                DynamicGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < world.RowsCount; i++)
            {
                DynamicGrid.RowDefinitions.Add(new RowDefinition() { Height= new GridLength(CellSize) });
            }
        }

        void SetView()
        {
            DynamicGrid.Children.Clear();
            for (int i = 0; i < world.RowsCount; i++)
            {
                for (int j = 0; j < world.ColCount; j++)
                {
                    var newItem = new Rectangle() { Stroke = new SolidColorBrush(Colors.WhiteSmoke)};
                    newItem.PreviewMouseLeftButtonDown += NewItem_PreviewMouseLeftButtonDown;
                    if (world.Area[i, j])
                        newItem.Fill = new SolidColorBrush(Colors.Green);
                    else
                        newItem.Fill = new SolidColorBrush(Colors.Black);

                    Grid.SetRow(newItem, i);
                    Grid.SetColumn(newItem, j);
                    DynamicGrid.Children.Add(newItem);
                 }
            }
        }

        private void NewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (started)
                return;
            var rect = sender as Rectangle;
            var indexOfRect = DynamicGrid.Children.IndexOf(rect);
            var twoDimIndex = IndexOperations.OneDimToTwoDimIndex(indexOfRect, world.ColCount);
            world.Area[twoDimIndex.Item1, twoDimIndex.Item2] = !world.Area[twoDimIndex.Item1, twoDimIndex.Item2];
            if (world.Area[twoDimIndex.Item1, twoDimIndex.Item2])
                rect.Fill = new SolidColorBrush(Colors.Green);
            else
                rect.Fill = new SolidColorBrush(Colors.Black);
           // SetView();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StartGame();
        }
    }
}
