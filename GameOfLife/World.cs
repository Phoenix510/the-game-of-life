﻿
using System.Diagnostics;

namespace GameOfLife
{
    public class World
    {
        public World(bool[,] area)
        {
            Area = area;
        }

        public bool[,] Area { get; private set; }
        public long ColCount => Area.GetLength(1);
        public long RowsCount => Area.GetLength(0);

        public bool[,] Cycle()
        {
            bool[,] newArea = new bool[Area.GetLength(0), Area.GetLength(1)];
            for(int i=0; i< RowsCount-1; i++)
            {
                for (int j = 0; j < ColCount; j++)
                {
                    var cell = Area[i, j];
                    var noOfAliveNeigbours = GetAliveNeighboursCount(i, j);
                    if (!cell && noOfAliveNeigbours == 3)
                        newArea[i, j] = true;
                    else if (cell && (noOfAliveNeigbours == 2 || noOfAliveNeigbours ==3))
                        newArea[i, j] = true;
                    else
                        newArea[i, j] = false;
                }
            }
            Area = newArea;
            return Area;
        }

        public int GetAliveNeighboursCount(long row, long col)
        {
            int aliveNeighbours = 0;
            if(row-1 >= 0)
            {
                if (col - 1 >= 0 && Area[row - 1, col - 1])
                    aliveNeighbours++;
                if (Area[row-1, col])
                    aliveNeighbours++;
                if (col + 1 < ColCount-1 && Area[row - 1, col + 1])
                    aliveNeighbours++;
            }

            if (col - 1 >= 0 && Area[row, col-1])
                aliveNeighbours++;
            if (col + 1 < ColCount - 1 && Area[row, col +1])
                aliveNeighbours++;

            if(row+1 <= RowsCount - 1)
            {
                if (col - 1 >= 0 && Area[row + 1, col - 1])
                    aliveNeighbours++;
                if (Area[row +1, col])
                    aliveNeighbours++;
                if (col + 1 < ColCount - 1 && Area[row + 1, col + 1])
                    aliveNeighbours++;
            }

            return aliveNeighbours;
        }
    }
}
