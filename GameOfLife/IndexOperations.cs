﻿namespace GameOfLife
{
    public class IndexOperations
    {
        public static (long, long) OneDimToTwoDimIndex(long index, long colCount)
        {
            int row = 0;
            long indeer = index;

            while (true)
            {
                if (index < colCount-1)
                    break;
                index -= colCount;
                row++;
            }

            return (row, index);
        }
    }

}
