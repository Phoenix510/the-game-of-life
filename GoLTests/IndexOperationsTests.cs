﻿using GameOfLife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoLTests
{
    public class IndexOperationsTests
    {
        [Fact]
        public void Fourteen_index_is_translated_into_ThreeTwo_index()
        {
            var colCount = 4;
            var result = IndexOperations.OneDimToTwoDimIndex(14, colCount);

            Assert.Equal((3,2), result);
        }
    }
}
