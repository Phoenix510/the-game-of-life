﻿using GameOfLife;

namespace GoLTests
{
    public class NeighboursCountCalcTests
    {
        [Theory]
        [InlineData(0, 4, 1)]
        [InlineData(1, 4, 2)]
        [InlineData(2, 4, 1)]
        public void Count_of_neighbours_is_calculated_correctly(long row, long col, 
            int expectedResult)
        {
            var area = new bool[20, 20];
            area[0, 4] = true;
            area[1, 4] = true;
            area[2, 4] = true;
            var sut = new World(area);

            int countOfNeighbours = sut.GetAliveNeighboursCount(row, col);

            Assert.Equal(expectedResult, countOfNeighbours);
        }
    }
}
